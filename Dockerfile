#
# Ubuntu Node.js Dockerfile
#
# https://github.com/dockerfile/ubuntu/blob/master/Dockerfile
# https://docs.docker.com/examples/nodejs_web_app/
#

# Pull base image.
# FROM ubuntu:14.04 
FROM ubuntu:18.04

# Install Node.js
RUN apt-get update
RUN apt-get install -y sudo 
RUN apt-get install -y curl 
RUN curl --silent --location https://deb.nodesource.com/setup_10.x | sudo bash -
RUN apt-get install -y nodejs
RUN apt-get install -y build-essential

# Bundle app source
# Trouble with COPY http://stackoverflow.com/a/30405787/2926832
COPY ./ ./


# Install app dependencies
RUN npm install

# Binds to port 3001
EXPOSE  3001

#  Defines your runtime(define default command)
CMD ["npm", "start"]
