const secrets = require("../secrets");
module.exports = {
  port: 3001,
  host: "localhost",
  ...secrets
};
