const joi = require("@hapi/joi");
const db = require("../../services/db");

module.exports = {
  method: "POST",
  path: "/song/create",
  handler: async (request, h) => {
    const { title, bandID, key } = request.payload;
    await db.query(
      `insert into songs 
      ( title, bandID, key ) 
      values ( ?, ?, true )`,
      [title, bandID, key]
    );
    const [{ id }] = await db.query("select last_insert_id() as id");
    const [band] = await db.query(`select * from songs where id = ?`, [id]);
    return band;
  },
  options: {
    validate: {
      payload: {
        bandID: joi
          .number()
          .integer()
          .required(),
        title: joi
          .string()
          .min(3)
          .max(1000)
          .required(),
        key: joi
          .string()
          .min(3)
          .max(10)
          .required()
      }
    }
  }
};
