const joi = require("@hapi/joi");
const db = require("../../services/db");

module.exports = {
  method: "GET",
  path: "/song/findByBand",
  handler: async (request, h) => {
    const { bandID } = request.query;
    const songs = await db.query("select * from songs where bandID = ?", [
      bandID
    ]);
    return songs;
  },
  options: {
    validate: {
      query: {
        bandID: joi
          .number()
          .integer()
          .required()
      }
    }
  }
};
