module.exports = {
  method: "DELETE",
  path: "/membership/{id}",
  handler: async (request, h) => {
    const [membership] = await db.query(
      `select * from memberships where id = ?`,
      [id]
    );
    await db.query(
      `delete from memberships 
      where id = ?`,
      [id]
    );
    return membership;
  }
};
