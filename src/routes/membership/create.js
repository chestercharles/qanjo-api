const joi = require("@hapi/joi");
const db = require("../../services/db");

module.exports = {
  method: "POST",
  path: "/membership/create",
  handler: async (request, h) => {
    const { bandID, userID } = request.payload;
    await db.query(
      `insert into memberships 
      ( userID, bandID, admin ) 
      values ( ?, ?, true )`,
      [bandID, userID]
    );
    const [{ id }] = await db.query("select last_insert_id() as id");
    const [band] = await db.query(`select * from memberships where id = ?`, [
      id
    ]);
    return band;
  },
  options: {
    validate: {
      payload: {
        bandID: joi
          .number()
          .integer()
          .required(),
        userID: joi
          .number()
          .integer()
          .required()
      }
    }
  }
};
