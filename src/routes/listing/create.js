const joi = require("@hapi/joi");
const db = require("../../services/db");

module.exports = {
  method: "POST",
  path: "/listing /create",
  handler: async (request, h) => {
    const { songID, setlistID, ordering } = request.payload;
    await db.query(
      `insert into listings 
      ( songID, setlistID, ordering ) 
      values ( ?, ?, ? )`,
      [songID, setlistID, ordering]
    );
    const [{ id }] = await db.query("select last_insert_id() as id");
    const [band] = await db.query(`select * from bands where id = ?`, [id]);
    return band;
  },
  options: {
    validate: {
      payload: {
        songID: joi
          .number()
          .integer()
          .required(),
        setlistID: joi
          .number()
          .integer()
          .required(),
        ordering: joi
          .number()
          .integer()
          .required()
      }
    }
  }
};
