const joi = require("@hapi/joi");
const db = require("../../services/db");

module.exports = {
  method: "DELETE",
  path: "/listing/{id}",
  handler: async (request, h) => {
    const [listing] = await db.query(`select * from listings where id = ?`, [
      id
    ]);
    await db.query(
      `delete from listings 
      where id = ?`,
      [id]
    );
    return listing;
  }
};
