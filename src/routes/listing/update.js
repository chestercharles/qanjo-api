const joi = require("@hapi/joi");
const db = require("../../services/db");

module.exports = {
  method: "PUT",
  path: "/listing/{id}",
  handler: async (request, h) => {
    const { ordering } = request.payload;
    await db.query(
      `update listings 
      set ordering = ?
      where id = ?`,
      [ordering]
    );
    const [listing] = await db.query(`select * from bands where listings = ?`, [
      id
    ]);
    return listing;
  },
  options: {
    validate: {
      payload: {
        ordering: joi
          .number()
          .integer()
          .required()
      }
    }
  }
};
