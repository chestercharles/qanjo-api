const userRoutes = [require("./user/create"), require("./user/login")];

const bandRoutes = [
  require("./band/create"),
  require("./band/searchByName"),
  require("./band/index")
];

module.exports = [...userRoutes, ...bandRoutes];
