const joi = require("@hapi/joi");
const db = require("../../services/db");

module.exports = {
  method: "POST",
  path: "/band/create",
  handler: async (request, h) => {
    const { bandName } = request.payload;
    await db.query(
      `insert into bands 
      ( bandName ) 
      values ( ? )`,
      [bandName]
    );
    const [{ id }] = await db.query("select last_insert_id() as id");
    const [band] = await db.query(`select * from bands where id = ?`, [id]);
    return band;
  },
  options: {
    validate: {
      payload: {
        bandName: joi
          .string()
          .min(3)
          .max(1000)
          .required()
      }
    }
  }
};
