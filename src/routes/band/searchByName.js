const joi = require("@hapi/joi");
const db = require("../../services/db");

module.exports = {
  method: "GET",
  path: "/band/searchByName",
  handler: async (request, h) => {
    const { bandName } = request.query;
    const bands = await db.query(`select * from bands where bandName like ?`, [
      "%" + bandName + "%"
    ]);
    return bands;
  },
  options: {
    validate: {
      query: {
        bandName: joi
          .string()
          .min(3)
          .max(1000)
          .required()
      }
    }
  }
};
