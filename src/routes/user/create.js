const bcrypt = require("bcrypt");
const joi = require("@hapi/joi");
const db = require("../../services/db");

module.exports = {
  method: "POST",
  path: "/user/create",
  handler: async (request, h) => {
    const { username, email, password } = request.payload;
    const encryptedPassword = await bcrypt.hash(password, 12);
    await db.query(
      `insert into users 
      ( username, email, hashedPassword ) 
      values ( ?, ?, ? )`,
      [username, email, encryptedPassword]
    );
    const [{ id }] = await db.query("select last_insert_id() as id");
    const [user] = await db.query(`select * from users where id = ?`, [id]);
    return user;
  },
  options: {
    auth: false,
    validate: {
      payload: {
        username: joi
          .string()
          .min(3)
          .max(50)
          .required(),
        email: joi
          .string()
          .min(3)
          .max(50)
          .required(),
        password: joi
          .string()
          .min(3)
          .max(50)
          .required()
      }
    }
  }
};
