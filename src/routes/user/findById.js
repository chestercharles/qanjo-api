const joi = require("@hapi/joi");
const db = require("../../services/db");

module.exports = {
  method: "GET",
  path: "/user/{id}",
  handler: async (request, h) => {
    const { id } = request.params;
    const [user] = db.query(`select * from users where id = ?`, [id]);
    return user;
  },
  options: {
    validate: {
      query: {
        id: joi
          .number()
          .integer()
          .required()
      }
    }
  }
};
