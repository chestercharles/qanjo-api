const bcrypt = require("bcrypt");
const joi = require("@hapi/joi");
const db = require("../../services/db");
const createToken = require("../../auth/createToken");

module.exports = {
  method: "POST",
  path: "/user/login",
  handler: async (request, h) => {
    const { username, password } = request.payload;
    const [user] = await db.query(`select * from users where username = ?`, [
      username
    ]);
    if (user) {
      const valid = await bcrypt.compare(password, user.hashedPassword);
      if (valid) {
        const token = createToken(user);
        return {
          token,
          valid
        };
      }
    }
    return { token: null, valid: false };
  },
  options: {
    auth: false,
    validate: {
      payload: {
        username: joi
          .string()
          .min(3)
          .max(50)
          .required(),
        password: joi
          .string()
          .min(3)
          .max(50)
          .required()
      }
    }
  }
};
