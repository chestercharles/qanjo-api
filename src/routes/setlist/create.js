const joi = require("@hapi/joi");
const db = require("../../services/db");

module.exports = {
  method: "POST",
  path: "/setlist/create",
  handler: async (request, h) => {
    const { name, bandID } = request.payload;
    await db.query(
      `insert into setlists 
      ( name, bandID ) 
      values ( ?, ? )`,
      [name, bandID]
    );
    const [{ id }] = await db.query("select last_insert_id() as id");
    const [setlist] = await db.query(`select * from setlists where id = ?`, [
      id
    ]);
    return setlist;
  },
  options: {
    validate: {
      payload: {
        bandID: joi
          .number()
          .integer()
          .required(),
        name: joi
          .string()
          .min(3)
          .max(50)
          .required()
      }
    }
  }
};
