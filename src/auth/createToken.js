const jwt = require("jsonwebtoken");
const config = require("../config");

function createToken(user) {
  return jwt.sign({ id: user.id, username: user.username }, config.jwtKey, {
    algorithm: "HS256",
    expiresIn: "1h"
  });
}

module.exports = createToken;
