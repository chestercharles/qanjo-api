const db = require("../services/db");
const config = require("../config");

module.exports = {
  name: "jwt",
  scheme: "jwt",
  options: {
    key: config.jwtKey,
    validate: async function(decodedToken, request) {
      const users = await db.query(`select * from users where id = ?`, [
        decodedToken.id
      ]);
      return {
        isValid: users.length === 1
      };
    },
    verifyOptions: { algorithms: ["HS256"] }
  }
};
