const { expect } = require("chai");
const db = require("./db");

describe("db service", () => {
  it("should execute a query", async () => {
    await db.connect();
    const [result] = await db.query("select 'howdy' from dual");
    expect(result.howdy).to.equal("howdy");
    await db.end();
  });
});
