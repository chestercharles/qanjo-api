const mysql = require("mysql");
const config = require("../config");

let connection;

module.exports = {
  connect: function() {
    return new Promise((resolve, reject) => {
      connection = mysql.createConnection({
        host: config.dbHost,
        user: config.dbUser,
        password: config.dbPass,
        database: config.dbName,
        port: config.dbPort
      });
      connection.connect(err => {
        if (err) {
          console.log(err);
          return reject(err);
        }
        return resolve();
      });
    });
  },
  query: function(sql, params) {
    if (typeof params === "undefined") {
      params = [];
    }
    return new Promise(async (resolve, reject) => {
      connection.query(sql, params, (err, results, fields) => {
        if (err) {
          console.log(err);
          return reject(err);
        }
        return resolve(results);
      });
    });
  },
  end: function() {
    return new Promise((resolve, reject) => {
      connection.end(err => {
        if (err) {
          console.log(err);
          return reject(err);
        }
        return resolve();
      });
    });
  }
};
