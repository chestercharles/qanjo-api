const Hapi = require("@hapi/hapi");
const config = require("./config");
const db = require("./services/db");
const jwt = require("./auth/jwt.strategy");
const fs = require("fs");

const runServer = async () => {
  if (!fs.existsSync("secrets.js")) {
    throw new Error("secrets.js file is missing!");
  }

  const server = Hapi.server({
    port: config.port,
    host: config.process
  });

  // Load jwt auth
  await server.register(require("hapi-auth-jwt2"));
  server.auth.strategy(jwt.name, jwt.scheme, jwt.options);

  const useJWT = !(process.env.NO_JWT === "true");

  if (useJWT) {
    server.auth.default("jwt");
  }

  // Load routes
  require("./routes").forEach(route => server.route(route));

  // Open db connection
  await db.connect();

  await server.start();

  return server;
};

process.on("unhandledRejection", err => {
  console.log(err);
  process.exit(1);
});

process.on("exit", () => {
  db.end();
});

runServer().then(server => {
  console.log("Server running on %s", server.info.uri);
});
